package cz.app_vision.DobreRano.alarmclock;

import junit.framework.TestCase;

import java.util.Calendar;

/**
 * Created by Lubos Ilcik on 11/20/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class AlarmManagerHelperTest extends TestCase {
    private static final String TAG = "AlarmManagerHelperTest";
    private static final int SUNDAY = Calendar.SUNDAY;
    private static final int MONDAY = Calendar.MONDAY;
    private static final int TUESDAY = Calendar.TUESDAY;
    private static final int WEDNESDAY = Calendar.WEDNESDAY;
    private static final int THURSDAY = Calendar.THURSDAY;
    private static final int FRIDAY = Calendar.FRIDAY;
    private static final int SATURDAY = Calendar.SATURDAY;

    public void test_daysBetween() throws Exception {
        // out of range conditions
        assertEquals(-1, AlarmManagerHelper.daysBetween(0, 0));
        assertEquals(-1, AlarmManagerHelper.daysBetween(MONDAY, 0));
        assertEquals(-1, AlarmManagerHelper.daysBetween(0, MONDAY));
        assertEquals(-1, AlarmManagerHelper.daysBetween(8, 8));
        assertEquals(-1, AlarmManagerHelper.daysBetween(8, MONDAY));
        assertEquals(-1, AlarmManagerHelper.daysBetween(MONDAY, 8));
        assertEquals(-1, AlarmManagerHelper.daysBetween(-1, -1));
        assertEquals(-1, AlarmManagerHelper.daysBetween(MONDAY, -1));
        assertEquals(-1, AlarmManagerHelper.daysBetween(-1, MONDAY));

        // equal days
        for (int day = SUNDAY; day <= SATURDAY; day++) {
            assertEquals(0, AlarmManagerHelper.daysBetween(day, day));
        }

        // second day idx is greater
        for (int firstDay = SUNDAY; firstDay <= SATURDAY; firstDay++) {
            for (int secondDay = firstDay + 1; secondDay <= SATURDAY; secondDay++) {
                assertEquals(secondDay-firstDay, AlarmManagerHelper.daysBetween(firstDay, secondDay));
            }
        }

        // days from SATURDAY
        for (int day = SUNDAY; day < SATURDAY; day++) {
            assertEquals(day, AlarmManagerHelper.daysBetween(SATURDAY, day));
        }
        // days till SATURDAY
        for (int day = SUNDAY; day < SATURDAY; day++) {
            assertEquals(SATURDAY-day, AlarmManagerHelper.daysBetween(day, SATURDAY));
        }

        // second day idx is smaller
        // days till SUNDAY
        assertEquals(6, AlarmManagerHelper.daysBetween(MONDAY, SUNDAY));
        assertEquals(5, AlarmManagerHelper.daysBetween(TUESDAY, SUNDAY));
        assertEquals(4, AlarmManagerHelper.daysBetween(WEDNESDAY, SUNDAY));
        assertEquals(3, AlarmManagerHelper.daysBetween(THURSDAY, SUNDAY));
        assertEquals(2, AlarmManagerHelper.daysBetween(FRIDAY, SUNDAY));
        assertEquals(1, AlarmManagerHelper.daysBetween(SATURDAY, SUNDAY));
        // days till WEDNESDAY
        assertEquals(6, AlarmManagerHelper.daysBetween(THURSDAY, WEDNESDAY));
        assertEquals(5, AlarmManagerHelper.daysBetween(FRIDAY, WEDNESDAY));
        assertEquals(4, AlarmManagerHelper.daysBetween(SATURDAY, WEDNESDAY));

    }

    AlarmModel alarm;
    CalendarDate calendar;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        alarm = new AlarmModel();
        calendar = new CalendarDate();
    }

    public void test_isAlarmToBeSetForDayOfWeek_alarm_date_vs_calendar_date() throws Exception {
        alarm.isEnabled = true;

        // now is MON, 10:15
        calendar.day = MONDAY;
        calendar.hour = 10;
        calendar.minute = 15;
        // enabling alarm is not enough to trigger alarm!
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(MONDAY, alarm, calendar));

        // set alarm time for later today
        alarm.timeHour = 12;
        alarm.timeMinute = 30;
        // now alarm is to be triggered for MONDAY, but still not sufficiently configured!
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(MONDAY, alarm, calendar));

        // set alarm day to MONDAY
        // WARNING: YOU MUST USE day consts from AlarmModel class!!!!
        alarm.setRepeatingDay(/* >>> */ AlarmModel.MONDAY /* <<< */, true);
        // now alarm is evaluated as to be set
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(MONDAY, alarm, calendar));

        // alarm set for MONDAY MUST NOT BE triggered for other checking days but MONDAY on calendar day = MONDAY
        for (int checkingDay = SUNDAY; checkingDay <= SATURDAY; checkingDay++) {
            if (checkingDay == MONDAY) {
                assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
            } else {
                assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
            }
        }

        // must be triggered on every calendar day earlier than alarm date
        // calendar week starts on SUNDAY (!!!), thus there is only one day earlier than MONDAY -> SUNDAY
        calendar.day = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(MONDAY, alarm, calendar));

        calendar.day = SATURDAY;
        // alarm set for MONDAY, current day is SATURDAY and I'm checking if alarm is to be set today or later but still in this week
        // should be false since MONDAY is the next week
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(MONDAY, alarm, calendar));
        // on SATURDAY only the alarm for SUNDAY is to be set!!
        alarm.setRepeatingDay(AlarmModel.SUNDAY, true);
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(SUNDAY, alarm, calendar));

    }

    public void test_isAlarmToBeSetForDayOfWeek_calendar_and_alarm_dates_are_equal() throws Exception {

        // we check the alarm for this day
        final int checkingDay = MONDAY;

        // default values
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, new AlarmModel(), new CalendarDate()));

        // alarm on MON, 12:15
        alarm.setRepeatingDay(AlarmModel.MONDAY, true);
        alarm.timeHour = 12;
        alarm.timeMinute = 15;
        alarm.isEnabled = true;

        // calendar date MON
        calendar.day = Calendar.MONDAY;

        // calendar minute before alarm: MON, 12:05
        calendar.hour =  alarm.timeHour;
        calendar.minute = 5;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // calendar min after alarm: MON, 12:20
        calendar.hour =  alarm.timeHour;
        calendar.minute = 20;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // calendar hour before alarm: MON, 11:15
        calendar.hour = 11;
        calendar.minute = alarm.timeMinute;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // calendar hour after alarm: MON, 13:15
        calendar.hour = 13;
        calendar.minute = alarm.timeMinute;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        //edge cases
        alarm.timeHour = 23;
        alarm.timeMinute = 59;
        calendar.hour = 0;
        calendar.minute = 0;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        alarm.timeHour = 24;
        alarm.timeMinute = 0;
        calendar.hour = 0;
        calendar.minute = 0;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        alarm.timeHour = 0;
        alarm.timeMinute = 0;
        calendar.hour = 23;
        calendar.minute = 59;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

    }

    public void test_isAlarmToBeSetForDayOfWeek_calendar_date_is_earlier() throws Exception {
        int checkingDay;

        // alarm on MON, 07:15
        alarm.setRepeatingDay(AlarmModel.MONDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.isEnabled = true;

        // calendar date SUN, 12:00
        // I am setting alarm for 07:15 what's meant for MON morning!
        calendar.day = Calendar.SUNDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
        calendar.day = Calendar.SATURDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // this case simulates edge conditions: SAT idx=7, SUN idx=1
        // alarm on SUN, 07:15
        alarm.setRepeatingDay(AlarmModel.SUNDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.isEnabled = true;

        // calendar date SAT, 12:00
        // I am setting alarm for 07:15 what's meant for SUN morning!
        calendar.day = Calendar.SATURDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
        checkingDay = MONDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // check of alarm set couple of days ahead
        alarm.setRepeatingDay(AlarmModel.WEDNESDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.isEnabled = true;

        calendar.day = Calendar.MONDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = WEDNESDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

    }

    public void test_isAlarmToBeSetForDayOfWeek_calendar_day_is_later() throws Exception {
        int checkingDay;

        // alarm on MON, 07:15
        alarm.setRepeatingDay(AlarmModel.MONDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.isEnabled = true;

        // calendar date TUE, 12:00
        // I am setting alarm for 07:15 what's meant for MON morning!
        calendar.day = Calendar.TUESDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // calendar date WED, 12:00
        // I am setting alarm for 07:15 what's meant for MON morning!
        calendar.day = Calendar.WEDNESDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // calendar day is TUE ... SAT
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        for (int calendarDay = TUESDAY; calendarDay <= SATURDAY; calendarDay++) {
            calendar.day = calendarDay;
            assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
        }

        // calendar date SUN, 12:00
        // SUNDAY is logically earlier day than MONDAY!!!!!
        // I am setting alarm for 07:15 what's meant for MON morning!
        calendar.day = Calendar.SUNDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));


    }


    public void test_isAlarmToBeSetForDayOfWeek_repeating_alarm() throws Exception {
        int checkingDay;
        // enable alarm
        alarm.isEnabled = true;

        // alarm on TUE, 07:15
        alarm.setRepeatingDay(AlarmModel.TUESDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;

        // calendar date TUE, 12:00
        calendar.day = Calendar.TUESDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = TUESDAY;
        // false when not repeating
        assertEquals(false, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));
        // enable repeating
        alarm.repeatWeekly = true;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // alarm on MON, 07:15 & repeat
        alarm.setRepeatingDay(AlarmModel.MONDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.repeatWeekly = true;

        // calendar date SAT, 12:00
        calendar.day = Calendar.SATURDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // alarm on SUN, 07:15 & repeat
        alarm.setRepeatingDay(AlarmModel.SUNDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.repeatWeekly = true;

        // calendar date MON, 12:00
        calendar.day = Calendar.MONDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // alarm on SUN, 07:15 & repeat
        alarm.setRepeatingDay(AlarmModel.SUNDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.repeatWeekly = true;

        // calendar date SAT, 12:00
        calendar.day = Calendar.SATURDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

        // alarm on WED, 07:15 & repeat
        alarm.setRepeatingDay(AlarmModel.WEDNESDAY, true);
        alarm.timeHour = 7;
        alarm.timeMinute = 15;
        alarm.repeatWeekly = true;

        // calendar date FRI, 12:00
        calendar.day = Calendar.FRIDAY;
        calendar.hour = 12;
        calendar.minute = 0;
        checkingDay = WEDNESDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmToBeSetForDayOfWeek(checkingDay, alarm, calendar));

    }


    public void test_isAlarmDayTodayOrLaterInTheWeek() throws Exception {

        int alarmDay = MONDAY;
        int today = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        alarmDay = MONDAY;
        today = TUESDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        alarmDay = TUESDAY;
        today = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        alarmDay = MONDAY;
        today = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        alarmDay = TUESDAY;
        today = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        alarmDay = SUNDAY;
        today = SATURDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));

        // current solution allows to set alarm for SUNDAY only if today is SATURDAY!!!!!!!
        alarmDay = MONDAY;
        today = SATURDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));
    }

    public void test_isAlarmDayTodayOrEarlierInTheWeek() throws Exception {

        int alarmDay = MONDAY;
        int today = TUESDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));
        for (int day = TUESDAY; day < SATURDAY; day++) {
            assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, day));
        }

        alarmDay = MONDAY;
        today = SUNDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));

        // ???
        alarmDay = SUNDAY;
        today = MONDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));
        today = SATURDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));

        // ???
        alarmDay = SATURDAY;
        today = SUNDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));

    }

    public void test_isAlarmDayTodayOrLaterInTheWeek_isAlarmDayTodayOrEarlierInTheWeek() throws Exception {
        int alarmDay;
        int today;

        alarmDay = MONDAY;
        today = TUESDAY;
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));

        // ????
        alarmDay = SATURDAY;
        today = SUNDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));
        assertEquals(false, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));

        // ????
        alarmDay = SUNDAY;
        today = SATURDAY;
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrLaterInTheWeek(alarmDay, today));
        assertEquals(true, AlarmManagerHelper.isAlarmDayTodayOrEarlierInTheWeek(alarmDay, today));


    }


}

/*
    Alarm states:

    1. non-repeating alarm states
    a. pending
    b. fired and inactive


    2. repeating alarm states
    a. pending
    b. fired and to be rescheduled

    1.a. = 2.a.
    - alarm date is later than current date

    1.b.
    - alarm date is earlier than current date and alarm is not repeating

    2.b.
    - alarm date is earlier than current date and alarm is repeating

 */