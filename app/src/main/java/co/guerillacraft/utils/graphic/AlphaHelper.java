package co.guerillacraft.utils.graphic;

import android.os.Build;
import android.view.View;
import android.view.animation.AlphaAnimation;

/**
 * Created by Lubos Ilcik on 10/29/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class AlphaHelper {

    public static void setAlphaForHoneycomb(View icon, float alpha) {
        final AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
        animation.setDuration(0);
        animation.setFillAfter(true);
        icon.startAnimation(animation);
    }

    public static void setAlpha(View v, float alpha) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            v.setAlpha(alpha);
        } else {
            final AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
            animation.setDuration(0);
            animation.setFillAfter(true);
            v.startAnimation(animation);
        }
    }


}
