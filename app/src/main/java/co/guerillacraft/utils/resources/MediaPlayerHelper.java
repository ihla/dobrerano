package co.guerillacraft.utils.resources;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

/**
 * Created by Lubos Ilcik on 11/18/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
// media player
public class MediaPlayerHelper {
    private final Context context;
    private final MediaStream mediaStream;
    final ResourceHelper resourceHelper;
    private MediaPlayer player;
    private boolean isPlaying = false;

    public enum MediaStream { DEFAULT, ALARM }

    public MediaPlayerHelper(Context context) {
        this(context, MediaStream.DEFAULT);
    }

    public MediaPlayerHelper(Context context, MediaStream mediaStream) {
        this.context = context;
        this.mediaStream = mediaStream;
        this.resourceHelper = new ResourceHelper(context);
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void playFromRawFile(String fileName) {
        int resourceId = resourceHelper.getRawResourceId(fileName);
        if (isPlaying) {
            player.stop();
            player.release();
        }
        isPlaying = true;
        if (mediaStream == MediaStream.DEFAULT) {
            player = MediaPlayer.create(context, resourceId);
            player.setOnCompletionListener(listener);
            player.start();
        } else if (mediaStream == MediaStream.ALARM) {
            player = new MediaPlayer();
            try {
                player.setDataSource(context, Uri.parse("android.resource://" + context.getPackageName() + "/" + resourceId));
                player.setAudioStreamType(AudioManager.STREAM_ALARM);
                player.setOnCompletionListener(listener);
                player.prepare();
                player.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e("MediaPlayerHelper", "unknown media stream type");
        }
    }

    public void release() {
        isPlaying = false;
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            player.release();
            player = null;
            isPlaying = false;
        }
    };

    public void stop() {
        if (player != null && isPlaying) {
            player.stop();
            isPlaying = false;
        }
    }

    public void resume() {
        if (player != null && !isPlaying) {
            player.start();
        }
    }
}