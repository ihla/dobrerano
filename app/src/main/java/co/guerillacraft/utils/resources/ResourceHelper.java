package co.guerillacraft.utils.resources;

import android.content.Context;

/**
 * Created by Lubos Ilcik on 10/30/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class ResourceHelper {
    private final Context context;

    public ResourceHelper(Context context) {
        this.context = context;
    }

    public int getRawResourceId(String fileName) {
        return context.getResources().getIdentifier(fileName, "raw", context.getPackageName());
    }

    public int getDrawableResourceId(String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }


}
