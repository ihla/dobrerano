package cz.app_vision.DobreRano;

/**
 * Created by Lubos Ilcik on 11/4/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class Constants {
    public static final float DIMMED_IMAGE_ALPHA = 0.4f;
    public static final float SELECTED_IMAGE_ALPHA = 1.0f;
}
