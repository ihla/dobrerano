package cz.app_vision.DobreRano.alarmclock;

import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmManagerHelper extends BroadcastReceiver {
    private static final String TAG = AlarmManagerHelper.class.getSimpleName();
    private static final boolean APPDEBUG = false;

	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String TIME_HOUR = "timeHour";
	public static final String TIME_MINUTE = "timeMinute";
	public static final String TONE = "alarmTone";
	
	@Override
	public void onReceive(Context context, Intent intent) {
        if (APPDEBUG) Log.d(TAG, ".onReceive: action=" + (intent.getAction() == null ? "null" : intent.getAction()));
        setAlarms(context);
	}
	
	public static void setAlarms(Context context) {
        if (APPDEBUG) Log.d(TAG, ".setAlarms");
		cancelAlarms(context);
		
		AlarmDBHelper dbHelper = new AlarmDBHelper(context);

		List<AlarmModel> alarms =  dbHelper.getAlarms();
        if (alarms == null) {
            // nothing to set
            return;
        }
		
		for (AlarmModel alarm : alarms) {
			if (alarm.isEnabled) {


                // TODO clean up this mess
                // the calendar value is not used for alarm time setting because of issue with time calc (see comment below)
                // the reason was missing setTimeInMillis() as appended below later on
                // now the value calculated by calculateAlarmTimeInMillis() and Calendar seem to be same but more testing is needed!
				Calendar calendar = Calendar.getInstance();
                // in order to get correct time in millis, you need to set it first
                calendar.setTimeInMillis(System.currentTimeMillis());
				calendar.set(Calendar.HOUR_OF_DAY, alarm.timeHour);
				calendar.set(Calendar.MINUTE, alarm.timeMinute);
				calendar.set(Calendar.SECOND, 0);

				// Find next time to set
                // I am passing POJO instead of Calendar object for the ease of testing
                final Calendar calendarToday = Calendar.getInstance();
				final int nowDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
				final int nowHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
				final int nowMinute = Calendar.getInstance().get(Calendar.MINUTE);
                final CalendarDate today = new CalendarDate();
                today.day = calendarToday.get(Calendar.DAY_OF_WEEK);
                today.hour = calendarToday.get(Calendar.HOUR_OF_DAY);
                today.minute = calendarToday.get(Calendar.MINUTE);

                boolean alarmSet = false;
                int daysToElapse;
                long alarmTimeInMillis;
                // check if alarm to be set for next day first
				for (int dayOfWeek = Calendar.SUNDAY; dayOfWeek <= Calendar.SATURDAY; ++dayOfWeek) { // iterate through all days in the week
                    if (isAlarmToBeSetTodayOrLaterInTheWeek(dayOfWeek, alarm, today)) {
                        daysToElapse = daysBetween(nowDay, dayOfWeek);
                        alarmTimeInMillis = calculateAlarmTimeInMillis(alarm.timeHour - nowHour, alarm.timeMinute - nowMinute, daysToElapse);
                        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
                        if (alarmTimeInMillis < Calendar.getInstance().getTimeInMillis()) Log.e(TAG, "FATAL ERROR: calendar time in millis is incorrect!");
                        PendingIntent intent = createPendingIntent(context, alarm);
						setAlarmInMillis(context, calendar /* only for tests to compare calc values */, alarmTimeInMillis, intent);
// The calculation of alarm time in millis by use of calendar value yields incorrect values when current day is 1 (SUNDAY) and alarm time is in the past, no idea why!!!
//						setAlarm(context, calendar, intent);
                        alarmSet = true;
                        if (APPDEBUG && !alarmSet) Log.d(TAG, "-> regular alarm set for day=" + dayOfWeek);
                        break;
					}
                }
                // check if repeating alarm
                // TODO remove duplicated code
                if (!alarmSet) {
                    for (int dayOfWeek = Calendar.SUNDAY; dayOfWeek <= Calendar.SATURDAY; ++dayOfWeek) { // iterate through all days in the week
                        if (isRepeatingAlarmToBeSet(dayOfWeek, alarm, today)) {
                            daysToElapse = daysBetween(nowDay, dayOfWeek);
                            // correct days to elapse for repeat weekly
                            if (daysToElapse == 0) {
                                // alarm is to be repeated in a week
                                daysToElapse = 7;
                            }
                            alarmTimeInMillis = calculateAlarmTimeInMillis(alarm.timeHour - nowHour, alarm.timeMinute - nowMinute, daysToElapse);
                            calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
                            if (alarmTimeInMillis < Calendar.getInstance().getTimeInMillis()) Log.e(TAG, "FATAL ERROR: calendar time in millis is incorrect!");
                            PendingIntent intent = createPendingIntent(context, alarm);
                            setAlarmInMillis(context, calendar /* only for tests to compare calc values */, alarmTimeInMillis, intent);
// The calculation of alarm time in millis by use of calendar value yields incorrect values when current day is 1 (SUNDAY) and alarm time is in the past, no idea why!!!
//						setAlarm(context, calendar, intent);
                            alarmSet = true;
                            if (APPDEBUG && !alarmSet) Log.d(TAG, "-> repeating alarm set for day=" + dayOfWeek);
                            break;
                        }
                    }
                }
                if (APPDEBUG && !alarmSet) Log.d(TAG, "alarm not set: (alarm d/h/m, current d/h/m))=" +
                        "./" + alarm.timeHour + "/" + alarm.timeMinute + " ," +
                        nowDay + "/" + nowHour + "/" + nowMinute);

			}
		}
	}

    public static int daysBetween(int firstDay, int secondDay) {
        // check out of range
        if (firstDay <= 0 || secondDay <= 0) {
            return -1;
        }
        if (firstDay > Calendar.SATURDAY || secondDay > Calendar.SATURDAY) {
            return -1;
        }

        int daysBetween;
        if (firstDay == secondDay) {
            daysBetween = 0;
        } else if (secondDay > firstDay) {
            daysBetween = secondDay - firstDay;
        } else { // secondDay < firstDay
            int daysTillSaturday = Calendar.SATURDAY - firstDay;
            daysBetween = daysTillSaturday + secondDay;
        }
        return daysBetween;
    }

    public static int getAlarmDate(int alarmHour, int alarmMinute) {
        final Calendar today = Calendar.getInstance();
        final int actualDay = today.get(Calendar.DAY_OF_WEEK);
        final int actualHour = today.get(Calendar.HOUR_OF_DAY);
        final int actualMinute = today.get(Calendar.MINUTE);

        int alarmDay;
        if (alarmHour < actualHour) {
            // alarm for next day
            alarmDay = actualDay + 1;
        } else if (alarmHour > actualHour) {
            // alarm for today
            alarmDay = actualDay;
        } else if (actualMinute >= alarmMinute) {
            // alarm for next day
            alarmDay = actualDay + 1;
        } else {
            // alarm for today
            alarmDay = actualDay;
        }
        return (alarmDay > 7 ? 1 : alarmDay);
    }

    /**
     * Checks if alarm is to be set for given day of week depending on the alarm configuration and calendar date.
     * <p>
     * Alarm will be set only if the alarm date not passed yet and the alarm day is today or later in the week.
     * @param dayOfWeek checking day, used as the index into the array of alarm days and for evaluation if it's today or later in the week
     * @param alarm alarm configuration incl. alarm date
     * @param today current calendar day
     * @return true if alarm is to be set today for the given alarm date
     */
    public static boolean isAlarmToBeSetForDayOfWeek(final int dayOfWeek, final AlarmModel alarm, final CalendarDate today) {

        boolean isToBeSet = isAlarmToBeSetTodayOrLaterInTheWeek(dayOfWeek, alarm, today);

        if (isToBeSet == false) {
            // check repeating alarm
            isToBeSet = isRepeatingAlarmToBeSet(dayOfWeek, alarm, today);
        }
        return isToBeSet;
    }

    public static boolean isAlarmToBeSetTodayOrLaterInTheWeek(final int dayOfWeek, final AlarmModel alarm, final CalendarDate today) {
        final int nowDay = today.day;
        final int nowHour = today.hour;
        final int nowMinute = today.minute;

        final boolean isAlarmScheduledForDay = alarm.getRepeatingDay(dayOfWeek - 1); // array is starting at idx=0!
        final boolean isDayToday = dayOfWeek == nowDay;
        boolean isAlarmDayTodayOrLaterInTheWeek = isAlarmDayTodayOrLaterInTheWeek(dayOfWeek, nowDay);
        final boolean isAlarmHourPassed = alarm.timeHour < nowHour;
        final boolean isAlarmHourNow = alarm.timeHour == nowHour;
        final boolean isAlarmMinutePassedOrNow = alarm.timeMinute <= nowMinute;

        final boolean isAlarmForTodayNotPassedYet = !(isDayToday && isAlarmHourPassed) && !(isDayToday && isAlarmHourNow && isAlarmMinutePassedOrNow);

        boolean isToBeSet = alarm.isEnabled && isAlarmScheduledForDay && isAlarmDayTodayOrLaterInTheWeek && isAlarmForTodayNotPassedYet;
        return isToBeSet;
    }

    public static boolean isRepeatingAlarmToBeSet(final int dayOfWeek, final AlarmModel alarm, final CalendarDate today) {
        final int nowDay = today.day;
        final boolean isAlarmScheduledForDay = alarm.getRepeatingDay(dayOfWeek - 1); // array is starting at idx=0!
        boolean isToBeSet = alarm.isEnabled && alarm.repeatWeekly && isAlarmScheduledForDay && isAlarmDayTodayOrEarlierInTheWeek(dayOfWeek, nowDay);
        return isToBeSet;
    }

    public static boolean isAlarmToBeSet(final AlarmModel alarm) {
        final Calendar calendar = Calendar.getInstance();
        final CalendarDate today = new CalendarDate();
        today.day = calendar.get(Calendar.DAY_OF_WEEK);
        today.hour = calendar.get(Calendar.HOUR_OF_DAY);
        today.minute = calendar.get(Calendar.MINUTE);

        boolean isToBeSet = false;
        int dayOfWeek;
        for (dayOfWeek = Calendar.SUNDAY; dayOfWeek <= Calendar.SATURDAY; ++dayOfWeek) {
            if (isToBeSet = isAlarmToBeSetForDayOfWeek(dayOfWeek, alarm, today)) {
                break;
            }
        }
        if (APPDEBUG) Log.d(TAG, "isAlarmToBeSet: " + (isToBeSet ? ("day=" + dayOfWeek + " " + true) : "" + false));
        return isToBeSet;
    }

    private static long calculateAlarmTimeInMillis(final long hoursToElapse, final long minutesToElapse, final long daysToElapse) {
        if (APPDEBUG) Log.d(TAG, "-> time to elapse: " + daysToElapse + " days + " + hoursToElapse + " hours + " + minutesToElapse + " mins");
        final long millisecondsPerMinute = 60 * 1000;

        // time to elapse since now
        final long deltaTimeToElapseInMillis = ((daysToElapse * 24 + hoursToElapse) * 60 + minutesToElapse) * millisecondsPerMinute;
        final double currentTimeSince1970InMillis = Calendar.getInstance().getTimeInMillis();
        // since alarm time is set to hours and minutes, ignore seconds in the current time and round to minutes
        final double currentTimeRoundToMin = currentTimeSince1970InMillis - currentTimeSince1970InMillis % millisecondsPerMinute;

        // time to elapse in millis (since 1970)
        return deltaTimeToElapseInMillis + (long) currentTimeRoundToMin;
    }

    @SuppressLint("NewApi")
	private static void setAlarm(Context context, final Calendar calendar, final PendingIntent pIntent) {
        if (APPDEBUG) Log.d(TAG, "-> setAlarm: d=" + calendar.get(Calendar.DAY_OF_WEEK) +
                " h=" + calendar.get(Calendar.HOUR_OF_DAY) +
                " m=" + calendar.get(Calendar.MINUTE) +
                " ms=" + calendar.getTimeInMillis() +
                " curr=" + Calendar.getInstance().getTimeInMillis());
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
		} else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
		}
	}

    @SuppressLint("NewApi")
    private static void setAlarmInMillis(Context context, final Calendar calendar, final long timeInMillis, final PendingIntent intent) {
        if (APPDEBUG) Log.d(TAG, "-> setAlarm: d=" + calendar.get(Calendar.DAY_OF_WEEK) +
                " h=" + calendar.get(Calendar.HOUR_OF_DAY) +
                " m=" + calendar.get(Calendar.MINUTE) +
                " curr-ms=" + Calendar.getInstance().getTimeInMillis() +
                " alarm-ms=" + timeInMillis +
                " cal-alarm-ms=" + calendar.getTimeInMillis());
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, intent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, intent);
        }
    }

    public static void cancelAlarms(Context context) {
        if (APPDEBUG) Log.d(TAG, ".cancelAlarms");
		AlarmDBHelper dbHelper = new AlarmDBHelper(context);
		
		List<AlarmModel> alarms =  dbHelper.getAlarms();
		
 		if (alarms != null) {
			for (AlarmModel alarm : alarms) {
				if (alarm.isEnabled) {
					PendingIntent pIntent = createPendingIntent(context, alarm);
	                if (APPDEBUG) Log.d(TAG, "-> cancelling");
					AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
					alarmManager.cancel(pIntent);
				}
			}
 		}
	}

	private static PendingIntent createPendingIntent(Context context, final AlarmModel model) {
		Intent intent = new Intent(context, AlarmService.class);
		intent.putExtra(ID, model.id);
		intent.putExtra(NAME, model.name);
		intent.putExtra(TIME_HOUR, model.timeHour);
		intent.putExtra(TIME_MINUTE, model.timeMinute);
		intent.putExtra(TONE, model.fileName);
        if (APPDEBUG) Log.d(TAG, ".createPendingIntent: id=" + model.id + " repeating weekly=" + model.repeatWeekly);
		return PendingIntent.getService(context, (int) model.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

    public static boolean isAlarmDayTodayOrLaterInTheWeek(int day, int today) {
        boolean isDayTodayOrLaterInTheWeek;
        if (today < Calendar.SATURDAY) { // today SUNDAY ... FRIDAY
            // if today == SUNDAY any day is evaluated as being today or later in the week!
            // this is definitely valid for calendar week starting on SUNDAY
            // and handles correctly the case when today=SUNDAY and alarm day=MONDAY
            isDayTodayOrLaterInTheWeek = day >= today;
        } else { // today == SATURDAY (7)
            // this branch is valid when the 1st day of week is Monday!!!!
            // and handles correctly the case when today=SATURDAY and alarm day=SUNDAY (or SATURDAY)
            isDayTodayOrLaterInTheWeek = day == Calendar.SATURDAY || day == Calendar.SUNDAY; // if today == SATURDAY, till the end of week remains only SUNDAY
        }
        return isDayTodayOrLaterInTheWeek;
    }

    public static boolean isAlarmDayTodayOrEarlierInTheWeek(int day, int today) {
        boolean isAlarmDay = day <= today;
        return isAlarmDay;
    }
}
