package cz.app_vision.DobreRano.alarmclock;

public class AlarmModel {

	public static final int SUNDAY = 0;
	public static final int MONDAY = 1;
	public static final int TUESDAY = 2;
	public static final int WEDNESDAY = 3;
	public static final int THURSDAY = 4;
	public static final int FRIDAY = 5;
	public static final int SATURDAY = 6;
	
	public long id = -1;
	public int timeHour;
	public int timeMinute;
	private boolean repeatingDays[];
	public boolean repeatWeekly;
	public String name;
	public boolean isEnabled;
    public String interpreter;
    public String fileName;

    public AlarmModel() {
		repeatingDays = new boolean[7];
    }

    public void setRepeatEveryDay() {
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            repeatingDays[dayOfWeek] = true;
        }
    }

    public boolean isRepeatingEveryDay() {
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            if (!repeatingDays[dayOfWeek]) return false;
        }
        return true;
    }

    public void clearRepeatingDays() {
        repeatingDays = new boolean[7];
    }

    public void setRepeatingDay(int dayOfWeek, boolean value) {
		repeatingDays[dayOfWeek] = value;
	}
	
	public boolean getRepeatingDay(int dayOfWeek) {
		return repeatingDays[dayOfWeek];
	}
	
}
