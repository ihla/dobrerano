package cz.app_vision.DobreRano.alarmclock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import cz.app_vision.DobreRano.app.AlarmScreenActivity;

public class AlarmService extends Service {

	private static final String TAG = AlarmService.class.getSimpleName();
    private static final boolean APPDEBUG = false;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (APPDEBUG) Log.d(TAG, ".onStartCommand");

        if (intent != null) {
            Intent alarmIntent = new Intent(getBaseContext(), AlarmScreenActivity.class);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            alarmIntent.putExtras(intent);
            getApplication().startActivity(alarmIntent);
        } else {
            // this should not happen normally, intent is null if e.g. process is killed
            Log.e(TAG, "onStartCommand called with intent=null");
        }

		AlarmManagerHelper.setAlarms(this);
		
		return super.onStartCommand(intent, flags, startId);
	}
	
}