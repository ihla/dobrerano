package cz.app_vision.DobreRano.alarmclock;

/**
 * Created by Lubos Ilcik on 11/21/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class CalendarDate {
    public int day;
    public int hour;
    public int minute;
}
