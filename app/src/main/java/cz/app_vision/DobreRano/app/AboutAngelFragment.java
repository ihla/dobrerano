package cz.app_vision.DobreRano.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Lubos Ilcik on 11/4/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class AboutAngelFragment extends Fragment {
    public static final String TAG = AboutAngelFragment.class.getSimpleName();
    private static final boolean APPDEBUG = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (APPDEBUG) Log.d(TAG, ".onCreateView");
        return inflater.inflate(R.layout.about_angel, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (APPDEBUG) Log.d(TAG, ".onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        view.setTag(TAG);
        Button button = (Button) view.findViewById(R.id.home_web_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchWeb(v);
            }
        });
        button = (Button) view.findViewById(R.id.app_web_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchWeb(v);
            }
        });
//        button = (Button) view.findViewById(R.id.write_us_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                writeUs(v);
//            }
//        });
    }

    public void writeUs(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "andelskaposta@dobryandel.cz", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Dobré ráno, Dobrý anděli");
        startActivityWithCheck(Intent.createChooser(emailIntent, getResources().getString(R.string.send_email)));
    }

    public void launchWeb(View v) {
        int id = v.getId();
        if (APPDEBUG) Log.d(TAG, ".launchWeb " + id);
        Intent intent  = new Intent(Intent.ACTION_VIEW);
        if (id == R.id.home_web_button) {
            String addr = "dobryandel.cz";
            intent.setData(Uri.parse(getWebAddress(addr)));
        } else if (id == R.id.app_web_button) {
            String uriString = "market://details?id=" + "cz.tmobile.dobryandel";
            intent.setData(Uri.parse(uriString));
            startActivityWithCheck(intent);
        } else {
            Log.e(TAG, "FATAL ERROR: unknown web link id!");
        }
        startActivityWithCheck(intent);
    }

    private void startActivityWithCheck(Intent intent) {
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast toast = Toast.makeText(getActivity(), R.string.activity_start_error, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private String getWebAddress(String s) {
        return String.format("http://www.%s", s);
    }


}
