package cz.app_vision.DobreRano.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Lubos Ilcik on 11/4/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class AboutApplicationFragment extends Fragment {
    public static final String TAG = AboutApplicationFragment.class.getSimpleName();
    private static final boolean APPDEBUG = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (APPDEBUG) Log.d(TAG, ".onCreateView");
        return inflater.inflate(R.layout.about_application, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (APPDEBUG) Log.d(TAG, ".onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        view.setTag(TAG);
    }

}
