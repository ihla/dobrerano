package cz.app_vision.DobreRano.app;

import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.*;
import co.guerillacraft.utils.resources.MediaPlayerHelper;
import cz.app_vision.DobreRano.Constants;
import cz.app_vision.DobreRano.alarmclock.AlarmDBHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmManagerHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmModel;

import java.util.ArrayList;

public class AlarmDetailsActivity extends ActionBarActivity {
    private static final String TAG = AlarmDetailsActivity.class.getSimpleName();
    private static final boolean APPDEBUG = false;
    public static final String ID_PARAM = "id";
    public static final String NAME_PARAM = "name";
    public static final String EDIT_PARAM = "edit";

    private AlarmDBHelper dbHelper = new AlarmDBHelper(this);
	
	private AlarmModel alarmDetails;
	
	private TimePicker timePicker;
	private EditText edtMessage;
//	private SwitchCompat chkRepeat;
    
    private ArrayList<View> images;
    private int selectedId = -1;
    private TextView selectedImageName;
    private String interpreter;
    private HorizontalScrollView scrollView;
    private ArrayList<Integer> drawableIds;
    private boolean editMode;
    private MediaPlayerHelper player;
    private ArrayList<View> buttonUnderlineViews;
    private ArrayList<Button> dayButtons;
    private boolean isRepeatingDay[] = new boolean[7];

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        if (APPDEBUG) Log.d(TAG, ".onCreate");

		setContentView(R.layout.activity_details);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
        images = getImageViews();
        drawableIds = getDrawableIds();
        setDrawables();
		timePicker = (TimePicker) findViewById(R.id.alarm_details_time_picker);
        timePicker.setIs24HourView(true);
		edtMessage = (EditText) findViewById(R.id.alarm_details_message);
//		chkRepeat = (SwitchCompat) findViewById(R.id.alarm_details_repeat);
        selectedImageName = (TextView) findViewById(R.id.selected_image_name);
        scrollView = (HorizontalScrollView) findViewById(R.id.photo_roll);

        dayButtons = getDayButtons();
        buttonUnderlineViews = getButtonUnderlineViews();

        Bundle extras = getIntent().getExtras();
        long id = extras.getLong(ID_PARAM);
        if (getIntent().hasExtra(EDIT_PARAM)) {
            getSupportActionBar().setTitle(R.string.edit_title);
            editMode = true;
        } else {
            getSupportActionBar().setTitle(R.string.action_add_content);
        }

		if (id == -1) {
			alarmDetails = new AlarmModel();
            interpreter = extras.getString(NAME_PARAM);
            if (interpreter != null && !interpreter.isEmpty()) {
                selectedImageName.setText(interpreter);
                selectedId = highlightImage(interpreter);
                scrollToSelectedId(selectedId);
            } else {
                highlightAllImages();
            }
		} else {
			alarmDetails = dbHelper.getAlarm(id);
            updateLayoutFromModel(alarmDetails);
		}

        // set hw controls
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        player = new MediaPlayerHelper(this);

    }

    private ArrayList<Button> getDayButtons() {
        ArrayList<Button> buttons = new ArrayList<Button>();
        buttons.add((Button)findViewById(R.id.day_0));
        buttons.add((Button)findViewById(R.id.day_1));
        buttons.add((Button)findViewById(R.id.day_2));
        buttons.add((Button)findViewById(R.id.day_3));
        buttons.add((Button)findViewById(R.id.day_4));
        buttons.add((Button)findViewById(R.id.day_5));
        buttons.add((Button)findViewById(R.id.day_6));
        return buttons;
    }

    private ArrayList<View> getButtonUnderlineViews() {
        ArrayList<View> views = new ArrayList<View>();
        views.add(findViewById(R.id.underline_0));
        views.add(findViewById(R.id.underline_1));
        views.add(findViewById(R.id.underline_2));
        views.add(findViewById(R.id.underline_3));
        views.add(findViewById(R.id.underline_4));
        views.add(findViewById(R.id.underline_5));
        views.add(findViewById(R.id.underline_6));
        return views;
    }

    public void selectDay(View v) {
        String idxString = v.getContentDescription().toString();
        int idx = Integer.valueOf(idxString);
        Button button = (Button) v;
        if (isRepeatingDay[idx]) {
            isRepeatingDay[idx] = false;
            button.setTextColor(getResources().getColor(R.color.day_unselected));
            buttonUnderlineViews.get(idx).setVisibility(View.INVISIBLE);
        } else {
            isRepeatingDay[idx] = true;
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderlineViews.get(idx).setVisibility(View.VISIBLE);
        }
    }


    // TODO trying to allocate 2985996 bytes of memory!!!!!
    private void setDrawables() {
        int i = 0;
        for (View image : images) {
            ((ImageView)image).setImageResource(drawableIds.get(i++));
        }
    }

    private void scrollToSelectedId(final int id) {
        scrollView.post(new Runnable() {

            @Override
            public void run() {
                final View view = findViewById(id);
                int scrollTo = 0;
                final int count = ((LinearLayout) scrollView.getChildAt(0)).getChildCount();
                int width = 0;
                for (int i = 0; i < count; i++) {
                    final View child = ((LinearLayout) scrollView.getChildAt(0)).getChildAt(i);
                    if (child != view) {
                        width = child.getWidth();
                        scrollTo += width;
                    } else {
                        // correct by one position to left
                        scrollTo -=width;
                        break;
                    }
                }
                scrollView.scrollTo(scrollTo, 0);
            }
        });
    }

    private int highlightImage(String imageName) {
        int id = -1;
        for (View v : images) {
            if (v.getContentDescription().toString().equals(imageName)) {
                id = v.getId();
                setAlpha(v, Constants.SELECTED_IMAGE_ALPHA);
            } else {
                setAlpha(v, Constants.DIMMED_IMAGE_ALPHA);
            }
        }
        return id;
    }

    private void highlightAllImages() {
        for (View v : images) {
            setAlpha(v, Constants.SELECTED_IMAGE_ALPHA);
        }
    }

    private void updateLayoutFromModel(AlarmModel alarmModel) {
        timePicker.setCurrentMinute(alarmModel.timeMinute);
        timePicker.setCurrentHour(alarmModel.timeHour);
        edtMessage.setText(alarmModel.name);
        interpreter = retrieveInterpreterName(alarmModel.fileName);
        if (!interpreter.isEmpty()) {
            selectedImageName.setText(interpreter);
            selectedId = highlightImage(interpreter);
            scrollToSelectedId(selectedId);
        } else {
            highlightAllImages();
        }
        updateButtons(alarmModel);
    }

    private void updateButtons(AlarmModel alarmModel) {
        if (!alarmModel.repeatWeekly) {
            return;
        }
        Button button;
        View buttonUnderline;
        if (alarmModel.getRepeatingDay(AlarmModel.MONDAY)) {
            button = dayButtons.get(0);
            buttonUnderline = buttonUnderlineViews.get(0);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[0] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.TUESDAY)) {
            button = dayButtons.get(1);
            buttonUnderline = buttonUnderlineViews.get(1);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[1] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.WEDNESDAY)) {
            button = dayButtons.get(2);
            buttonUnderline = buttonUnderlineViews.get(2);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[2] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.THURSDAY)) {
            button = dayButtons.get(3);
            buttonUnderline = buttonUnderlineViews.get(3);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[3] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.FRIDAY)) {
            button = dayButtons.get(4);
            buttonUnderline = buttonUnderlineViews.get(4);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[4] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.SATURDAY)) {
            button = dayButtons.get(5);
            buttonUnderline = buttonUnderlineViews.get(5);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[5] = true;
        }
        if (alarmModel.getRepeatingDay(AlarmModel.SUNDAY)) {
            button = dayButtons.get(6);
            buttonUnderline = buttonUnderlineViews.get(6);
            button.setTextColor(getResources().getColor(R.color.day_selected));
            buttonUnderline.setVisibility(View.VISIBLE);
            isRepeatingDay[6] = true;
        }
    }

    private String retrieveInterpreterName(String name) {
        if (name.isEmpty()) {
            return name;
        }

        String[] interpreters = getResources().getStringArray(R.array.interpreters_alphabetically);
        return interpreters[getInterpreterNameIdx(name)];
    }

    private int getInterpreterNameIdx(String interpreterName) {
        String[] names = getResources().getStringArray(R.array.names_alphabetically);
        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            if (name.equals(interpreterName)) {
                return i;
            }
        }
        return -1;
    }

    private ArrayList<View> getImageViews() {
        ArrayList<View> images = new ArrayList<View>();
        images.add(findViewById(R.id.brano_holicek));
        images.add(findViewById(R.id.bara_hrzanova));
        images.add(findViewById(R.id.emma_smetana));
        images.add(findViewById(R.id.eva_samkova));
        images.add(findViewById(R.id.hana_maciuchova));
        images.add(findViewById(R.id.hanka_vagnerova));
        images.add(findViewById(R.id.jakub_kohak));
        images.add(findViewById(R.id.jaromir_bosak));
        images.add(findViewById(R.id.klara_oltova));
        images.add(findViewById(R.id.lukas_pollert));
        images.add(findViewById(R.id.martin_stransky));
        images.add(findViewById(R.id.matej_ruppert));
        images.add(findViewById(R.id.manicka_hurvinek));
        images.add(findViewById(R.id.pavel_liska));
        images.add(findViewById(R.id.petr_koukal));
        images.add(findViewById(R.id.roman_vojtek));
        images.add(findViewById(R.id.simona_stasova));
        images.add(findViewById(R.id.spejbl_hurvinek));
        images.add(findViewById(R.id.vojta_kotek));
        return images;
    }

    private ArrayList<Integer> getDrawableIds() {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        ids.add(R.drawable.brano_holicek);
        ids.add(R.drawable.bara_hrzanova);
        ids.add(R.drawable.emma_smetana);
        ids.add(R.drawable.eva_samkova);
        ids.add(R.drawable.hana_maciuchova);
        ids.add(R.drawable.hanka_vagnerova);
        ids.add(R.drawable.jakub_kohak);
        ids.add(R.drawable.jaromir_bosak);
        ids.add(R.drawable.klara_oltova);
        ids.add(R.drawable.lukas_pollert);
        ids.add(R.drawable.martin_stransky);
        ids.add(R.drawable.matej_ruppert);
        ids.add(R.drawable.manicka_hurvinek);
        ids.add(R.drawable.pavel_liska);
        ids.add(R.drawable.petr_koukal);
        ids.add(R.drawable.roman_vojtek);
        ids.add(R.drawable.simona_stasova);
        ids.add(R.drawable.spejbl_hurvinek);
        ids.add(R.drawable.vojta_kotek);
        return ids;
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.alarm_details, menu);
		return true;
	}

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.action_delete).setVisible(editMode);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				break;
			}
			case R.id.action_save_alarm_details: {
				updateModelFromLayout();

				AlarmManagerHelper.cancelAlarms(this);

				if (alarmDetails.id < 0) {
					dbHelper.createAlarm(alarmDetails);
				} else {
					dbHelper.updateAlarm(alarmDetails);
				}

				AlarmManagerHelper.setAlarms(this);

				setResult(RESULT_OK);
				finish();
                break;
			}
            case R.id.action_delete:
                delete();
                setResult(RESULT_OK);
                finish();
                break;
		}

		return super.onOptionsItemSelected(item);
	}

    public void onImageButtonClick(View v) {
        if (APPDEBUG) Log.d(TAG, ".onImageButtonClick");
        if (v.getId() == selectedId) {
            highlightAllImages();
            selectedId = -1;
            interpreter = null;
            selectedImageName.setText(R.string.selected_image_name_default_text);
            player.release();
        } else {
            swapSelectedImage(v);
            selectedId = v.getId();
            interpreter = v.getContentDescription().toString();
            selectedImageName.setText(interpreter);
            scrollToSelectedId(selectedId);
            player.playFromRawFile(getInterpreterAudioFileName(interpreter));
        }

    }

    private void delete() {
        //Cancel Alarms
        AlarmManagerHelper.cancelAlarms(this);
        //Delete alarm from DB by id
        dbHelper.deleteAlarm(alarmDetails.id);
        //Set the alarms
        AlarmManagerHelper.setAlarms(this);
    }

    private void swapSelectedImage(View v) {
        for (View image : images) {
            if (image.equals(v)) {
                setAlpha(image, Constants.SELECTED_IMAGE_ALPHA);
            } else {
                setAlpha(image, Constants.DIMMED_IMAGE_ALPHA);
            }
        }
    }

    private void setAlpha(View v, float alpha) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            v.setAlpha(alpha);
        } else {
            // TODO solution for image button state
            final AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
            animation.setDuration(0);
            animation.setFillAfter(true);
            v.startAnimation(animation);
        }
    }

    private void updateModelFromLayout() {
		alarmDetails.timeMinute = timePicker.getCurrentMinute();
		alarmDetails.timeHour = timePicker.getCurrentHour();
        if (setAlarmDetailsRepeatingDays()) {
            alarmDetails.repeatWeekly = true;
        } else {
            alarmDetails.repeatWeekly = false;
            setAlarmDetailsDate(alarmDetails.timeHour, alarmDetails.timeMinute);
        }
        alarmDetails.name = edtMessage.getText().toString();
		alarmDetails.isEnabled = true;
        String interpreterName = getInterpreterName();
        alarmDetails.interpreter = interpreterName;
        alarmDetails.fileName = getInterpreterAudioFileName(interpreterName);
	}

    private boolean setAlarmDetailsRepeatingDays() {
        boolean isSet = false;
        alarmDetails.clearRepeatingDays();
        if (isRepeatingDay[0]) {
            alarmDetails.setRepeatingDay(AlarmModel.MONDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[1]) {
            alarmDetails.setRepeatingDay(AlarmModel.TUESDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[2]) {
            alarmDetails.setRepeatingDay(AlarmModel.WEDNESDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[3]) {
            alarmDetails.setRepeatingDay(AlarmModel.THURSDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[4]) {
            alarmDetails.setRepeatingDay(AlarmModel.FRIDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[5]) {
            alarmDetails.setRepeatingDay(AlarmModel.SATURDAY, true);
            isSet = true;
        }
        if (isRepeatingDay[6]) {
            alarmDetails.setRepeatingDay(AlarmModel.SUNDAY, true);
            isSet = true;
        }

        return isSet;
    }

    private String getInterpreterName() {
        return interpreter != null ? interpreter : getRandomInterpreter();
    }

    private String getInterpreterAudioFileName(String interpreter) {
        return ((DobreRanoApplication)getApplication()).getInterpreterAudioFileName(interpreter);
    }

    private String getRandomInterpreter() {
        return ""; // returns empty string
    }

    private void setAlarmDetailsDate(int alarmHour, int alarmMinute) {
        int alarmDay = AlarmManagerHelper.getAlarmDate(alarmHour, alarmMinute);
        alarmDetails.clearRepeatingDays();
        alarmDetails.setRepeatingDay(alarmDay - 1, true);
    }

    @Override
    protected void onDestroy() {
        if (APPDEBUG) Log.d(TAG, ".onDestroy");
        images = null; // dealoc heap
        player.release();

        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (APPDEBUG) Log.d(TAG, ".onLowMemory");
        super.onLowMemory();
    }
}
