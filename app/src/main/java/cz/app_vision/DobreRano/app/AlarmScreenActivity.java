package cz.app_vision.DobreRano.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import co.guerillacraft.utils.resources.MediaPlayerHelper;
import co.guerillacraft.utils.resources.ResourceHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmDBHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmManagerHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmModel;

import java.util.Random;

public class AlarmScreenActivity extends Activity {
	
	public final String TAG = this.getClass().getSimpleName();
    private static final boolean APPDEBUG = false;

	private WakeLock mWakeLock;
	private MediaPlayerHelper player;

	private static final int WAKELOCK_TIMEOUT = 60 * 1000;
    private String file;

    private static final int[] postponeValues = new int[] {5, 10, 15, 20, 25, 30}; // in minutes
    private long alarmId;
    private Handler handler = new Handler();
    private Runnable releaseWakelock = new Runnable() {

        @Override
        public void run() {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

            if (mWakeLock != null && mWakeLock.isHeld()) {
                mWakeLock.release();
                if (APPDEBUG) Log.d(TAG, ".run: wakelock released");
            }
        }
    };
    private String message;
    private int timeHour;
    private int timeMinute;
    private ImageView thumbnailImageView;
    private TextView messageTextView;
    private TextView alarmTimeTextView;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Setup layout
		this.setContentView(R.layout.activity_alarm_screen);

        player = new MediaPlayerHelper(this, MediaPlayerHelper.MediaStream.ALARM);

        extractData(getIntent());
        if (APPDEBUG) Log.d(TAG, ".onCreate: alarmId=" + alarmId + " file=" + file + " h=" + timeHour + " m=" + timeMinute);

        thumbnailImageView = (ImageView) findViewById(R.id.interpreter_image);
        messageTextView = (TextView) findViewById(R.id.alarm_screen_name);
        alarmTimeTextView = (TextView) findViewById(R.id.alarm_screen_time);

		Button dismissButton = (Button) findViewById(R.id.dismiss_button);
		dismissButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                player.stop();
                // player will be released in onPause()
                finish();
            }
        });
        Button postponeButton = (Button) findViewById(R.id.postpone_button);
        postponeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // change request - postpone by 5 min by default
                postponeAlarmBy(5);
//                startDialogMenu();
            }
        });

        // set hw controls
        setVolumeControlStream(AudioManager.STREAM_ALARM);
        // set audio volume acc. to stored preferences
        int alarmVolume = getSharedPreferences(DobreRanoApplication.SHARED_PREFS, 0).getInt(DobreRanoApplication.SHARED_PREFS_ALARM_VOLUME, -1);
        if (alarmVolume != -1) {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, alarmVolume, 0);
        }

	}

    private void updateGUI() {
        thumbnailImageView.setImageResource(new ResourceHelper(this).getDrawableResourceId(file));
        messageTextView.setText(message);
        alarmTimeTextView.setText(String.format("%02d : %02d", timeHour, timeMinute));
    }

    private void extractData(Intent intent) {
        message = intent.getStringExtra(AlarmManagerHelper.NAME);
        timeHour = intent.getIntExtra(AlarmManagerHelper.TIME_HOUR, 0);
        timeMinute = intent.getIntExtra(AlarmManagerHelper.TIME_MINUTE, 0);
        file = intent.getStringExtra(AlarmManagerHelper.TONE).isEmpty() ? getRandom() : intent.getStringExtra(AlarmManagerHelper.TONE);
        alarmId = intent.getLongExtra(AlarmManagerHelper.ID, -1000);
    }

    private String getRandom() {
        String[] files = getResources().getStringArray(R.array.names_alphabetically);
        Random generator = new Random();
        int i = generator.nextInt(files.length);
        return files[i];
    }

    private boolean startDialogMenu() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.postpone_dialog_title)
                .setItems(R.array.postpone_dialog_items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        postponeAlarmBy(postponeValues[which]);
                    }
                })
                .setPositiveButton(R.string.cancel, null);
        builder.create().show();

        return true;
    }

    private void postponeAlarmBy(int minutes) {
        AlarmModel newAlarm = updateAlarm(minutes);
        showToast(newAlarm);
        setResult(RESULT_OK);
        finish();
    }

    private void showToast(AlarmModel newAlarm) {
        String text = String.format("Budík odložen na %02d : %02d", newAlarm.timeHour, newAlarm.timeMinute);
        Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
        toast.show();
    }

    private AlarmModel updateAlarm(int minutes) {
        AlarmDBHelper dbHelper = new AlarmDBHelper(this);
        AlarmModel model = dbHelper.getAlarm(alarmId);
        int newTimeMinute = model.timeMinute;
        newTimeMinute += minutes;
        if (newTimeMinute >= 60) {
            model.timeHour++;
            int overHour = newTimeMinute % 60;
            if (overHour > 0) {
                model.timeMinute = overHour;
            } else {
                model.timeMinute = 0;
            }
        } else {
            model.timeMinute = newTimeMinute;
        }

        if (dbHelper.updateAlarm(model) <= 0) {
            Log.e(TAG, "FATAL ERROR: updateAlarm() upon postpone failed");
        }
        AlarmManagerHelper.setAlarms(this);

        return model;
    }

    private void playAlarm() {
        //Play alarm file
        if (file != null && !file.equals("")) {
            player.playFromRawFile(file);
        }
    }

    @SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();
        if (APPDEBUG) Log.d(TAG, ".onResume");

        updateGUI();

		// Set the window to keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		// Acquire wakelock
		PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
		if (mWakeLock == null) {
			mWakeLock = pm.newWakeLock((PowerManager.FULL_WAKE_LOCK | PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), TAG);
		}

		if (!mWakeLock.isHeld()) {
			mWakeLock.acquire();
			if(APPDEBUG) Log.i(TAG, "wakelock aquired");
		}

        //Ensure wakelock release in case the activity is not finished within WAKELOCK_TIMEOUT
        handler.postDelayed(releaseWakelock, WAKELOCK_TIMEOUT);

        playAlarm();

	}

	@Override
	protected void onPause() {
		super.onPause();
        if (APPDEBUG) Log.d(TAG, ".onPause");

        if (player.isPlaying()) player.stop();
        player.release();

        if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
            if (APPDEBUG) Log.d(TAG, ".onPause: wakelock released");
		}
        if (handler != null) {
            handler.removeCallbacks(releaseWakelock);
        }
	}
}
