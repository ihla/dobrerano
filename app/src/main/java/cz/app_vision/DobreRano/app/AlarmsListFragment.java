package cz.app_vision.DobreRano.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.*;
import android.widget.*;
import co.guerillacraft.utils.graphic.AlphaHelper;
import cz.app_vision.DobreRano.Constants;
import cz.app_vision.DobreRano.alarmclock.AlarmDBHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmManagerHelper;
import cz.app_vision.DobreRano.alarmclock.AlarmModel;
import co.guerillacraft.utils.resources.ResourceHelper;

import java.util.List;

/**
 * Created by Lubos Ilcik on 10/30/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class AlarmsListFragment extends ListFragment {
    public static final String TAG = AlarmsListFragment.class.getSimpleName();
    private static final boolean APPDEBUG = false;

    private ListView listView;
    private AlarmDBHelper dbHelper;
    private AlarmListAdapter adapter;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton v, boolean checked) {
            if (v.getTag() != null) {
                AlarmModelHolder amHolder = (AlarmModelHolder) v.getTag();
                AlarmModel alarm = dbHelper.getAlarm(amHolder.model.id);
                final boolean isToBeActivated = checked && !isAlarmActive(alarm);
                if (alarm.isEnabled != checked || isToBeActivated) {
                    AlarmManagerHelper.cancelAlarms(getActivity());
                    alarm.isEnabled = checked;
                    if (checked && isToBeActivated && !alarm.repeatWeekly) {
                        // set for next day
                        alarm.setRepeatingDay(AlarmManagerHelper.getAlarmDate(alarm.timeHour, alarm.timeMinute) - 1, true);
                    }
                    dbHelper.updateAlarm(alarm);
                    AlarmManagerHelper.setAlarms(getActivity());
                    adapter.updateAlarm(amHolder.position, alarm);
                }
            } else {
                Log.e(TAG, "FATAL ERROR: getTag() should not return null!");
            }
        }
    };
    private boolean isContextualActionMode;

    private void setImageViewAlpha(ImageView view, boolean isHighlighted) {
        if (isHighlighted) {
            AlphaHelper.setAlpha(view, Constants.SELECTED_IMAGE_ALPHA);
        } else {
            AlphaHelper.setAlpha(view, Constants.DIMMED_IMAGE_ALPHA);
        }
    }

    private void setAlarmTextView(TextView view, boolean active) {
        if (active) {
            view.setTextColor(getActivity().getResources().getColor(R.color.text_alpha_87));
        } else {
            view.setTextColor(getActivity().getResources().getColor(R.color.text_alpha_54));
        }
    }
    private void setInterpreterNameTextView(TextView view, boolean active) {
        if (active) {
            view.setTextColor(getActivity().getResources().getColor(R.color.interpreter_name));
        } else {
            view.setTextColor(getActivity().getResources().getColor(R.color.text_alpha_54));
        }
    }

    private android.support.v7.view.ActionMode actionMode;
    private android.support.v7.view.ActionMode.Callback actionModeCallback = new android.support.v7.view.ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            isContextualActionMode = true;
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_delete:
                    deleteSelectedItem(selectedPosition);
                    actionMode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.action_edit:
                    startAlarmEditActivityWithPosition(selectedPosition);
                    actionMode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            listView.setItemChecked(selectedPosition, false);
            isContextualActionMode = false;
            AlarmsListFragment.this.actionMode = null;
        }
    };

    private void startAlarmEditActivityWithPosition(int selectedPosition) {
        Intent intent = new Intent(getActivity(), AlarmDetailsActivity.class);
        long id = adapter.getAlarm(selectedPosition).id;
        intent.putExtra(AlarmDetailsActivity.ID_PARAM, id);
        intent.putExtra(AlarmDetailsActivity.EDIT_PARAM, true);
        startActivityForResult(intent, 0);
    }

    private int selectedPosition;

    private void deleteSelectedItem(int position) {
        //Cancel Alarms
        AlarmManagerHelper.cancelAlarms(getActivity());
        //Delete alarm from DB by id
        dbHelper.deleteAlarm(adapter.getAlarm(position).id);
        //Remove selected row
        adapter.removeAlarm(position);
        //Set the alarms
        AlarmManagerHelper.setAlarms(getActivity());
    }

    private AdapterView.OnItemLongClickListener onLongClick = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            if (APPDEBUG) Log.d(TAG, ".onItemLongClick");

            selectedPosition = position;
            listView.setItemChecked(position, true);
            boolean ret;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                ret = startDialogMenu();
            } else {
                ret = startActionMode();
            }
            return ret;
        }
    };

    private boolean startDialogMenu() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.edit_title)
                .setItems(R.array.context_menu, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0: // edit
                                startAlarmEditActivityWithPosition(selectedPosition);
                                break;
                            case 1: // delete
                                deleteSelectedItem(selectedPosition);
                                break;
                            default:
                                break;
                        }
                    }
                })
                .setPositiveButton(R.string.cancel, null);
        builder.create().show();

        return true;
    }

    private boolean startActionMode() {
        if (actionMode != null) {
            return false;
        }
        actionMode = ((ActionBarActivity)getActivity()).startSupportActionMode(actionModeCallback);
        return true;
    }

    public static AlarmsListFragment newInstance(Bundle args) {
        AlarmsListFragment fragment = new AlarmsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AlarmsListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            // no args needed yet
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dbHelper = new AlarmDBHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.alarm_list, container, false);
        view.setTag(TAG);
        listView = (ListView) view.findViewById(android.R.id.list);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setOnItemLongClickListener(onLongClick);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.alarm_list_footer, listView, false);
        listView.addFooterView(footer, null, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // not optimal to set adapter here but this only works for this app with API 10
        List<AlarmModel> alarms = dbHelper.getAlarms();
        if (alarms != null) {
            adapter = new AlarmListAdapter(getActivity(), alarms);
            setListAdapter(adapter);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (!isContextualActionMode) {
            AlarmModel model = adapter.getAlarm(position);
            startAlarmEditActivityWithId(model.id);
        }
    }


    private void setAlarmEnabled(long id, boolean isEnabled) {

        AlarmModel model = dbHelper.getAlarm(id);
        if (model.isEnabled != isEnabled) {
            AlarmManagerHelper.cancelAlarms(getActivity());
            model.isEnabled = isEnabled;
            dbHelper.updateAlarm(model);
            AlarmManagerHelper.setAlarms(getActivity());
        }

    }

    private void startAlarmEditActivityWithId(long id) {
        Intent intent = new Intent(getActivity(), AlarmDetailsActivity.class);
        intent.putExtra(AlarmDetailsActivity.ID_PARAM, id);
        intent.putExtra(AlarmDetailsActivity.EDIT_PARAM, true);
        startActivityForResult(intent, 0);
    }

    private boolean isAlarmActive(AlarmModel model) {
        return AlarmManagerHelper.isAlarmToBeSet(model);
    }

    private static class AlarmModelHolder {
        public AlarmModel model;
        int position;
    }

    private class AlarmListAdapter extends BaseAdapter {

        private final LayoutInflater inflater;
        private final List<AlarmModel> alarms;
        private ResourceHelper resourceHelper;

        public AlarmListAdapter(Context context, List<AlarmModel> alarms) {
            this.alarms = alarms;
            inflater = LayoutInflater.from(context);
            resourceHelper = new ResourceHelper(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            AlarmModelHolder amHolder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.alarm_list_item, parent, false);
                holder = new ViewHolder();
                holder.profilePhoto = (ImageView) convertView.findViewById(R.id.alarm_item_interpreter_image);
                holder.interpreterName = (TextView) convertView.findViewById(R.id.interpreter_name);
                holder.alarmTime = (TextView) convertView.findViewById(R.id.alarm_item_time);
                holder.alarmSwitch = (SwitchCompat) convertView.findViewById(R.id.alarm_item_switch);
                holder.alarmSwitch.setOnCheckedChangeListener(onCheckedChangeListener);

                amHolder = new AlarmModelHolder();
                holder.alarmSwitch.setTag(amHolder);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
                amHolder = (AlarmModelHolder) holder.alarmSwitch.getTag();
            }
            AlarmModel model = getAlarm(position);
            holder.profilePhoto.setImageResource(getDrawableResourceId(model.fileName)); //drawable image name is the same as audio file name!
            String name = model.fileName.isEmpty() ? getString(R.string.random_interpreter) : retrieveInterpreterName(model.fileName);
            holder.interpreterName.setText(name);

            final boolean alarmActive = isAlarmActive(model);
            setImageViewAlpha(holder.profilePhoto, alarmActive);
            holder.alarmTime.setText(String.format("%02d : %02d", model.timeHour, model.timeMinute));
            setAlarmTextView(holder.alarmTime, alarmActive);
            setInterpreterNameTextView(holder.interpreterName, alarmActive);

            amHolder.model = model;
            amHolder.position = position;
            holder.alarmSwitch.setTag(amHolder);
            holder.alarmSwitch.setChecked(alarmActive);

            return convertView;
        }

        private int getDrawableResourceId(String fileName) {
            return !fileName.isEmpty() ? resourceHelper.getDrawableResourceId(fileName) : R.drawable.placeholder;
        }

        private String retrieveInterpreterName(String name) {
            if (name.isEmpty()) {
                return name;
            }

            String[] interpreters = getResources().getStringArray(R.array.interpreters_alphabetically);
            return interpreters[getInterpreterNameIdx(name)];
        }

        private int getInterpreterNameIdx(String interpreterName) {
            String[] names = getResources().getStringArray(R.array.names_alphabetically);
            for (int i = 0; i < names.length; i++) {
                String name = names[i];
                if (name.equals(interpreterName)) {
                    return i;
                }
            }
            return -1;
        }


        @Override
        public int getCount() {
            return alarms.size();
        }

        @Override
        public Object getItem(int position) {
            return alarms.get(position);
        }

        AlarmModel getAlarm(int position) { return alarms.get(position); }


        @Override
        public long getItemId(int position) {
            return alarms.get(position).id;
        }

        public void updateAlarm(int position, AlarmModel alarm) {
            alarms.set(position, alarm);
            notifyDataSetChanged();
        }

        private class ViewHolder {
            public ImageView profilePhoto;
            public TextView alarmTime;
            public SwitchCompat alarmSwitch;
            public TextView interpreterName;
        }

        public void removeAlarm(int position) {
            alarms.remove(position);
            notifyDataSetChanged();
        }
    }

}
