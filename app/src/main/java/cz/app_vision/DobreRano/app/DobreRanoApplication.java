package cz.app_vision.DobreRano.app;

import android.app.Application;
import android.util.Log;
import cz.app_vision.DobreRano.model.Interpreter;
import cz.app_vision.DobreRano.model.Names;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lubos Ilcik on 10/29/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class DobreRanoApplication extends Application {

    public static final String SHARED_PREFS = "DobreRano.Settings";
    private static final String TAG = DobreRanoApplication.class.getSimpleName();
    private static final boolean APPDEBUG = false;
    public static final String SHARED_PREFS_ALARM_VOLUME = "alarm_volume";
    private Names names;

    @Override
    public void onCreate() {
        super.onCreate();
        names = Names.create(this);
//        names = Names.createFromJSON(loadAssetsJSONFile("names.json"));
    }

    public String getInterpreterAudioFileName(String interpreter) {
        return names.getInterpreterrAudioFileName(interpreter);
    }

    public List<Interpreter> getInterpretersAlphabetically() {
        ArrayList<Interpreter> interpreters = new ArrayList<Interpreter>();
        String[] names = getResources().getStringArray(R.array.interpreters_alphabetically);
        String[] fileNames = getResources().getStringArray(R.array.names_alphabetically);
        int i = 0;
        for (String name : names) {
            Interpreter interpreter = new Interpreter();
            interpreter.name = name;
            interpreter.fileName = fileNames[i++];
            interpreters.add(interpreter);
        }
        return interpreters;
    }

    private String loadAssetsJSONFile(String fileName) {
        String json = "";
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            int ret = is.read(buffer);
            is.close();
            json = (ret >= 0) ? new String(buffer, "UTF-8") : null;
        } catch (IOException ex) {
            if (APPDEBUG) Log.e(TAG, ".loadAssetsJSONFile " + ex);
            if (APPDEBUG) ex.printStackTrace();
        }
        return json;
    }

}
