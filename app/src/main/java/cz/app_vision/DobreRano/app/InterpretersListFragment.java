package cz.app_vision.DobreRano.app;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import co.guerillacraft.utils.resources.MediaPlayerHelper;
import co.guerillacraft.utils.resources.ResourceHelper;
import cz.app_vision.DobreRano.model.Interpreter;

import java.util.List;

/**
 * Created by Lubos Ilcik on 10/30/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class InterpretersListFragment extends ListFragment {

    public static final String TAG = InterpretersListFragment.class.getSimpleName();
    private static final boolean APPDEBUG = false;

    private List<Interpreter> interpreters;
    private InterpretersListAdapter adapter;
    private int selectedPosition = -1;
    private View selectedView;
    private MediaPlayerHelper player;
    private View.OnClickListener addAlarm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            if (APPDEBUG) Log.d(TAG, ".onClick: pos=" + position);
            Interpreter interpreter = adapter.getItem(position);
            startAddAlarmActivity(interpreter.name);
            player.release();
        }
    };

    private void startAddAlarmActivity(String name) {
        Intent intent = new Intent(getActivity(), AlarmDetailsActivity.class);
        long id = -1;
        intent.putExtra(AlarmDetailsActivity.ID_PARAM, id);
        intent.putExtra(AlarmDetailsActivity.NAME_PARAM, name);
        startActivityForResult(intent, 0);
    }

    public static InterpretersListFragment newInstance(Bundle args) {
        InterpretersListFragment fragment = new InterpretersListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public InterpretersListFragment () {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            // no args needed yet
        }

        // set hw controls
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

        player = new MediaPlayerHelper(getActivity());

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setTag(TAG);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        interpreters =  ((DobreRanoApplication)getActivity().getApplication()).getInterpretersAlphabetically();
        adapter = new InterpretersListAdapter(getActivity(), interpreters);
        setListAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        player.release();
        super.onDestroyView();
    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        selectedPosition = position;
        if (selectedView != null) {
            selectedView.setBackgroundColor(getResources().getColor(R.color.background_color));
        }
        selectedView = v;
        v.setBackgroundColor(getResources().getColor(R.color.selected_list_highlight));
        Interpreter interpreter = adapter.getItem(position);
        play(interpreter);
    }

    private void play(Interpreter interpreter) {
        player.playFromRawFile(interpreter.fileName);
    }

    private class InterpretersListAdapter extends ArrayAdapter<Interpreter> {

        private final LayoutInflater inflater;

        public InterpretersListAdapter(Context context, List<Interpreter> interpreters) {
            super(context, R.layout.interpreters_list_item, interpreters);
            inflater = LayoutInflater.from(context);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.interpreters_list_item, parent, false);
                holder = new ViewHolder();
                holder.interpreterName = (TextView) convertView.findViewById(R.id.interpreter_name);
                holder.profileImage = (ImageView) convertView.findViewById(R.id.interpreter_image);
                holder.addButton = (ImageView) convertView.findViewById(R.id.add_interpreter);
                holder.addButton.setOnClickListener(addAlarm);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.interpreterName.setText(getItem(position).name);
            holder.profileImage.setImageResource(getDrawableResourceId(getItem(position).fileName));
            holder.addButton.setTag(position);

            if (position == selectedPosition) {
                convertView.setBackgroundColor(getResources().getColor(R.color.selected_list_highlight));
            } else {
                convertView.setBackgroundColor(getResources().getColor(R.color.background_color));
            }

            return convertView;
        }

        private int getDrawableResourceId(String name) {
            return getActivity().getResources().getIdentifier(name, "drawable", getActivity().getPackageName());
        }

        private class ViewHolder {
            public TextView interpreterName;
            public ImageView profileImage;
            public ImageView addButton;
        }
    }

}
