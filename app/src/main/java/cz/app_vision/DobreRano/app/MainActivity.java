package cz.app_vision.DobreRano.app;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.*;
import android.widget.*;
import co.guerillacraft.utils.graphic.AlphaHelper;
import co.guerillacraft.utils.resources.MediaPlayerHelper;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final boolean APPDEBUG = false;
    private final String SELECTED_ITEM_KEY = "selected_item";

    private CharSequence drawerTitle;
    private CharSequence title;
    private String[] navigationListTitles;
    private DrawerLayout drawerLayout;
    private RelativeLayout drawerLayoutParent;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private String[] titles;
    private int selectedItem = 0;
    private int alarmVolume = -1;

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (APPDEBUG) Log.d(TAG, ".onCreate");
        setContentView(R.layout.activity_main);

        //setup navigation drawer
        title = drawerTitle = getTitle();
        navigationListTitles = getResources().getStringArray(R.array.navigation_list);
        titles = getResources().getStringArray(R.array.titles);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayoutParent = (RelativeLayout) findViewById(R.id.left_drawer_parent);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        // set a custom shadow that overlays the main content when the drawer opens
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        drawerList.setAdapter(new DrawerItemsListAdapter(this, navigationListTitles));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        // set alpha for < HONEYCOMB
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            AlphaHelper.setAlphaForHoneycomb(findViewById(R.id.drawer_logo_icon), 0.87f);
        }


        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_action_navigation_menu,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        if (savedInstanceState != null) {
            selectedItem = savedInstanceState.getInt(SELECTED_ITEM_KEY);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (APPDEBUG) Log.d(TAG, ".onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        // go to the main screen
        selectedItem = 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (APPDEBUG) Log.d(TAG, ".onSaveInstanceState");
        outState.putInt(SELECTED_ITEM_KEY, selectedItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (APPDEBUG) Log.d(TAG, ".onRestoreInstanceState");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (APPDEBUG) Log.d(TAG, ".onNewIntent");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (APPDEBUG) Log.d(TAG, ".onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (APPDEBUG) Log.d(TAG, ".onResume");
        selectItem(selectedItem);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (APPDEBUG) Log.d(TAG, ".onDestroy");
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerLayoutParent);
        menu.findItem(R.id.action_add_content).setVisible(!drawerOpen && selectedItem == 0);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add_content) {
            startAlarmDetailsActivity(-1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        getSupportActionBar().setTitle(title);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (APPDEBUG) Log.d(TAG, ".onPostCreate");
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (APPDEBUG) Log.d(TAG, ".onConfigurationChanged");
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }


    private void selectItem(int position) {
        if (APPDEBUG) Log.d(TAG, ".selectItem: " + position);
        if (position < 4) selectedItem = position; // not storred for settings!
        // update the main content by replacing fragments
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = null;
        switch (position) {
            case 0:
                if (fm.findFragmentByTag(AlarmsListFragment.TAG) == null) {
                    fragment = AlarmsListFragment.newInstance(null);
                }
                break;
            case 1:
                if (fm.findFragmentByTag(InterpretersListFragment.TAG) == null) {
                    fragment = InterpretersListFragment.newInstance(null);
                }
                break;
            case 2:
                if (fm.findFragmentByTag(AboutAngelFragment.TAG) == null) {
                    fragment = new AboutAngelFragment();
                }
                break;
            case 3:
                if (fm.findFragmentByTag(AboutApplicationFragment.TAG) == null) {
                    fragment = new AboutApplicationFragment();
                }
                break;
            case 4:
                showSettingsDialog();
                break;
            default:
                break;
        }
        if (fragment != null) fm.beginTransaction().replace(R.id.content_frame, fragment).commit();


        // update selected item and title, then close the drawer
        drawerList.setItemChecked(position, true);
        if (position < 4) setTitle(titles[position]); // settings have no title!!!
        drawerLayout.closeDrawer(drawerLayoutParent);
    }

    private void showSettingsDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View seekBarView = inflater.inflate(R.layout.set_audio_volume_dialog, (ViewGroup) findViewById(R.id.drawer_layout), false);
        SeekBar volumeSeekbar = (SeekBar) seekBarView.findViewById(R.id.seek_bar);

        final MediaPlayerHelper player = new MediaPlayerHelper(this, MediaPlayerHelper.MediaStream.ALARM);

        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM));
        alarmVolume = getSharedPreferences(DobreRanoApplication.SHARED_PREFS, 0).getInt(DobreRanoApplication.SHARED_PREFS_ALARM_VOLUME, -1);
        if (alarmVolume == -1) {
            alarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        }
        volumeSeekbar.setProgress(alarmVolume);
        volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                alarmVolume = progress;
                audioManager.setStreamVolume(AudioManager.STREAM_ALARM, alarmVolume, 0);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!player.isPlaying()) {
                    player.playFromRawFile(getResources().getStringArray(R.array.names_alphabetically)[0]);
                }
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setView(seekBarView)
                .setTitle(getResources().getString(R.string.settings_dialog_title))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences prefs = getSharedPreferences(DobreRanoApplication.SHARED_PREFS, 0);
                        prefs.edit().putInt(DobreRanoApplication.SHARED_PREFS_ALARM_VOLUME, alarmVolume).apply();
                        player.release();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        player.release();
                    }
                });
        builder.create().show();
    }

    public void startAlarmDetailsActivity(long id) {
        if (APPDEBUG) Log.d(TAG, ".startAlarmDetailsActivity: id=" + id);
        Intent intent = new Intent(this, AlarmDetailsActivity.class);
        intent.putExtra(AlarmDetailsActivity.ID_PARAM, id);
        startActivityForResult(intent, 0);
    }

   private static class DrawerItemsListAdapter extends ArrayAdapter<String> {

       private final LayoutInflater inflater;
       private final ArrayList<Integer> icons;

       public DrawerItemsListAdapter(Context context, String[] items) {
           super(context, R.layout.drawer_list_item, items);
           inflater = LayoutInflater.from(context);
           icons = new ArrayList<Integer>();
           icons.add(R.drawable.ic_action_alarm);
           icons.add(R.drawable.ic_social_person_outline);
           icons.add(R.drawable.ic_action_info_outline);
           icons.add(R.drawable.ic_action_info_outline);
           icons.add(R.drawable.ic_image_tune);
       }

       @Override
       public View getView(int position, View convertView, ViewGroup parent) {
           ViewHolder holder;
           if (convertView == null) {
               convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
               holder = new ViewHolder();
               holder.icon = convertView.findViewById(R.id.item_icon);
               holder.text = (TextView) convertView.findViewById(R.id.label);
               convertView.setTag(holder);
           } else {
               holder = (ViewHolder)convertView.getTag();
           }
           holder.text.setText(getItem(position));
           holder.icon.setBackgroundResource(icons.get(position));
           if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
               AlphaHelper.setAlphaForHoneycomb(holder.icon, 0.87f);
           }

           return convertView;
       }

       private static class ViewHolder {
           View icon;
           TextView text;
       }
   }
}
