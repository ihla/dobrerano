package cz.app_vision.DobreRano.model;

import android.content.Context;
import cz.app_vision.DobreRano.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by Lubos Ilcik on 10/29/14.
 * Copyright (c) 2014 Guerillacraft, www.guerillacraft.co. All rights reserved.
 */
public class Names {

    private HashMap<String, String> map;

    public static Names createFromJSON(String json) {
        return new JsonParser().parse(json);
    }

    public static Names create(Context context) {
        Names names = new Names();
        String[] interpreters = context.getResources().getStringArray(R.array.interpreters_alphabetically);
        int i = 0;
        for (String key : context.getResources().getStringArray(R.array.names_alphabetically)) {
            names.map.put(key, interpreters[i++]);
        }
        return names;
    }

    private Names() {
        map = new HashMap<String, String>();
    }

    public String getInterpreterrAudioFileName(String interpreter) {
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> item : entrySet) {
            if (item.getValue().equals(interpreter)) {
                return item.getKey();
            }
        }
        return null;
    }

    public List<Interpreter> getInterpreters() {
        ArrayList<Interpreter> interpreters = new ArrayList<Interpreter>();
        for (String key : map.keySet()) {
            Interpreter interpreter = new Interpreter();
            interpreter.fileName = key;
            interpreter.name = map.get(key);
            interpreters.add(interpreter);
        }
        return interpreters;
    }
    private static class JsonParser {

        private static final String NAMES_KEY = "names";
        private static final String FILE_NAME_KEY = "file";
        private static final String NAME_KEY = "name";

        public Names parse(String json) {
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray jNames = getNamesJsonArray(jObject);
            return getNames(jNames);
        }

        private JSONArray getNamesJsonArray(JSONObject jObject) {
            JSONArray jNames = null;
            try {
                jNames = jObject.getJSONArray(NAMES_KEY);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jNames;
        }

        private Names getNames(JSONArray jNames) {
            Names names = new Names();
            for (int i=0; i < jNames.length(); i++) {
                try {
                    JSONObject jObject = (JSONObject) jNames.get(i);
                    names.map.put(getKey(jObject), getValue(jObject));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return names;
        }

        private String getKey(JSONObject jObject) {
            String key = "";
            try {
                key = jObject.getString(FILE_NAME_KEY);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return key;
        }

        private String getValue(JSONObject jObject) {
            String value = "";
            try {
                value = jObject.getString(NAME_KEY);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return value;
        }
    }
}
